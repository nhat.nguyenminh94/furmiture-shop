<div class="br-logo"><a href="{{route('shop.index')}}"><span>[</span>Sun<span>]</span></a></div>
<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
    <div class="br-sideleft-menu">
        <a href="/admin" class="br-menu-link active">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Trang chủ</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        {{--Product--}}
        <a href="#" class="br-menu-link">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
                <span class="menu-item-label">Quản lý sản phẩm</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{route('admin.categoryproduct.list')}}" class="nav-link">Danh mục sản
                    phẩm</a></li>
            <li class="nav-item"><a href="{{route('admin.product.list')}}" class="nav-link">Sản Phẩm</a></li>
        </ul>
        {{-- End Product--}}

        {{-- Article--}}
        <a href="#" class="br-menu-link">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
                <span class="menu-item-label">Quản lý tin tức</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{route('admin.categoryarticle.list')}}" class="nav-link">Danh mục tin tức</a>
            </li>
            <li class="nav-item"><a href="{{route('admin.article.list')}}" class="nav-link">Tin tức</a></li>
        </ul>
        {{-- End Article--}}

        {{--Order--}}
        <a href="#" class="br-menu-link">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
                <span class="menu-item-label">Quản lý đơn hàng</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <ul class="br-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{route('admin.order.add')}}" class="nav-link">Tạo đơn hàng mới</a>
            </li>
            <li class="nav-item"><a href="{{route('admin.order.list')}}" class="nav-link">Quản lý đơn hàng</a></li>
        </ul>
        {{--Order--}}
    </div><!-- br-sideleft-menu -->


    <br>
</div><!-- br-sideleft -->