@extends('admin.layouts.master')
@section('styles')
    <style type="text/css" media="screen">
        .table-bordered tr td, th {
            vertical-align: middle;
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">articles</h1>
    <p class="mb-4"></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">articles Table</h6>
        </div>
        <div class="card-header py-3">
            <div class="col-md-2">
                <a class="btn btn-success text-center" href="{{route('admin.article.add')}}"
                   style="width: 100%;margin-bottom: 10px;">
                    Add </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Sub Title</th>
                        <th>Image</th>
                        <th>status</th>
                        <th>Category Name</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Sub Title</th>
                        <th>Image</th>
                        <th>status</th>
                        <th>Category Name</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{$article->id}}</td>
                            <td>{{$article->title}}</td>
                            <td>{{$article->sub_title}}</td>

                            <td align="center">
                                <img src="/{{$article->images[0]->src ??  ''}}" class="img-thumbnail rounded"
                                     style="max-width: 150px;" alt="">
                            </td>
                            <td>@if($article->status == 1 )
                                    Show
                                @else
                                    Hidden
                                @endif</td>
                            <td>
                                {{$article->category->name ?? "Error Or No Name"}}
                            </td>
                            <td>
                                <a class="btn btn-primary" href="{{route('admin.article.edit',['id'=>$article->id])}}">Update</a>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger openModalDelete" data-toggle="modal"
                                        data-target="#exampleModalCenter" data-id="{{$article->id}}"
                                        data-action="{{route('admin.article.destroy',['id'=>$article->id])}}">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                {{ $articles->links() }}
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Nếu chắc chắn xóa vui lòng nhấn nút Delete <br>
                    <b style="color:#E02D1B;">Lưu ý khi xóa sản phẩm sẽ không thể phục hồi</b>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="" method="POST" class="deleteForm">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" id="idDelete" name="idDelete" value="">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/fm_money/simple.money.format.js"></script>
    <script type="text/javascript" charset="utf-8" async defer>

        $(document).ready(function () {
            $('.price_article').simpleMoneyFormat();
            // var money = $('.price_article').text();
            //       var money1 = $('.price_article').simpleMoneyFormat(money);
            //       console.log(money1);
            //       $('.price_article').html(money1);
        });

        $(document).on("click", ".openModalDelete", function () {
            var myBookId = $(this).data('id');
            var myUrl = $(this).data('action');
            $(".modal-content #idDelete").val(myBookId);
            $(".modal-footer .deleteForm").attr('action', myUrl);
            $(".modal-content #exampleModalLongTitle").html('Bạn Chắc Chắn muốn xóa sản phẩm số ' + myBookId + ' chứ ?');
            // As pointed out in comments,
            // it is unnecessary to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        });
    </script>

@endsection