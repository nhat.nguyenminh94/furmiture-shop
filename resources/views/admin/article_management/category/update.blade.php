@extends('admin.layouts.master')
@section('content')
	  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
           @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Update Category!</h1>
              </div>
              <form class="user" method="post" action="{{route('admin.categoryarticle.update',['id'=>$category->id])}}">
              	@csrf
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" id="" name="name" placeholder="Name" value="{{isset($category) ? $category->name : '' }}">
                </div>
                <div class="form-group row">
                    <legend class="col-form-label col-sm-2 pt-0">Status</legend>
                     <div class="col-sm-10">
                        <div class="form-check">
                              <input class="form-check-input" type="radio" name="status" id="gridRadios1"
                                     value="1" @if( isset($category) && $category->status == 1 )
                                     checked
                                     @elseif(!isset($category))
                                     checked
                                      @endif>
                              <label class="form-check-label" for="gridRadios1">
                                  Show
                              </label>
                          </div>
                          <div class="form-check">
                              <input class="form-check-input" type="radio" name="status" id="gridRadios2"
                                     value="2" @if(isset($category) && $category->status == 2 )
                                     checked
                                      @endif>
                              <label class="form-check-label" for="gridRadios2">
                                 Hidden
                              </label>
                          </div>
                      </div>
                  </div>
                <div class="form-group">
                 <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection