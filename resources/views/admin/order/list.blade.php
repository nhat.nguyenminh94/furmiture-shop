@extends('admin.layouts.master')
@section('styles')
@endsection
@section('content')
    <!-- Page Heading -->
 <!--    <h1 class="h3 mb-2 text-gray-800">Đơn hàng</h1>
    --> <p class="mb-4"></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Bảng đơn hàng</h6>
        </div>
        <div class="card-header py-3">
            <div class="col-md-2">
                <a class="btn btn-success text-center" href="{{route('admin.order.add')}}"
                   style="width: 100%;margin-bottom: 10px;">
                    Tạo đơn hàng mới  </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ngày Đặt Hàng</th>
                        <th>Tên người nhận</th>
                        <th>Số điện thoại nhận hàng</th>
                        <th>Trạng thái</th>
                        <th>Tổng tiền</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Ngày Đặt Hàng</th>
                        <th>Tên người nhận</th>
                        <th>Số điện thoại nhận hàng</th>
                        <th>Trạng thái</th>
                        <th>Tổng tiền</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{date('d-m-Y', strtotime($order->order_date))}}</td>
                            <td>{{$order->shipment_name}}</td>
                            <td>{{$order->phone}}</td>
                            <td>@if($order->status == 1 )
                                   Đang chờ xử lý đơn hàng
                                @elseif($order->status == 2 )
                                    Đã Giao Đơn hàng
                                @else
                                	Đã Hủy Đơn hàng
                                @endif</td>
                            <td>
                               {{ number_format($order->total,0,',','.') }}
                            </td>
                            <td>
                                <a class="btn btn-primary" href="{{route('admin.order.edit',['id'=>$order->id])}}">Update</a>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger openModalDelete" data-toggle="modal"
                                        data-target="#exampleModalCenter" data-id="{{$order->id}}"
                                        data-action="{{route('admin.order.destroy',['id'=>$order->id])}}">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection