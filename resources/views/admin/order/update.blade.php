@extends('admin.layouts.master')
@section('styles')
    <link rel='stylesheet' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'>
    <script src="/ckeditor/ckeditor.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@endsection
@section('content')
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Cập nhật đơn hàng</h1>
                            </div>
                            <form class="user" method="post" action="{{route('admin.order.created')}}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="">Địa chỉ giao hàng</label>
                                        <input type="text" class="form-control form-control-user" id=""
                                               name="shipment_address"
                                               value="{{old('shipment_address',isset($order->shipment_address) ? $order->shipment_address:'') }}" placeholder="Địa chỉ giao hàng">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="Tên người nhận">Tên người nhận</label>
                                        <input type="text" class="form-control form-control-user " id=""
                                               name="shipment_name" value="{{old('shipment_name',isset($order->shipment_name) ? $order->shipment_name:'') }}"
                                               placeholder="Tên người nhận">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="">Số điện thoại người nhận</label>
                                        <input type="text" class="form-control form-control-user" id="" name="phone"
                                               value="{{old('phone',isset($order->phone) ? $order->phone:'') }}" placeholder="Số điện thoại người nhận">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="Tên người nhận">Trạng Thái </label>
                                        <select class="form-control" name="status">
                                            <option value="1" {{isset($order) ? ($order->status == 1 ? 'selected' : '') : '' }}>Đang xử lý</option>
                                            <option value="2" {{isset($order) ? ($order->status == 2 ? 'selected' : '') : '' }}>Đã giao</option>
                                            <option value="3" {{isset($order) ? ($order->status == 3 ? 'selected' : '') : '' }}>Đã hủy</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="">Ngày nhận hàng</label>
                                        <input type="datetime-local" class="form-control form-control-user" id="" name="received_date"
                                               value="{{old('received_date',isset($order->received_date) ? $order->received_date:'') }}" placeholder="Ngày nhận hàng">
                                    </div>
                                </div>
                                <div class="row" id="row-create-product">
                                    <div class="form-group col-md-6">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                                data-target="#exampleModalCenter">Tìm kiếm sản phẩm
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Tạo Đơn</button>
                                </div>
                            </form>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal find product -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Tìm kiếm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row ">
                        <div class="form-group col-md-12 row productForm">
                            <div class="col-md-4">
                                <label for="">Tên sản phẩm </label>
                                <input type="text" class="form-control form-control-user find-product-name"
                                       id="product-name" name="product-name"
                                       value="" placeholder="Tên Sản phẩm">
                            </div>
                            <div class=" col-md-5">
                                <label for="Tên người nhận">Danh mục </label>
                                <select class="form-control find-category_product_id" name="find-category_product_id">
                                    <option value="0">Tất cả danh mục</option>
                                    }
                                    option
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 align-bottom ">
                                <label for="Tên người nhận">Chọn </label>
                                <button type="button" class="btn btn-info btn-sm btn-block btn-find-product">Tìm kiếm
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <table class="table table-bordered" id="tab-search">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Tên SP</th>
                                <th scope="col">Giá</th>
                                <th scope="col">Loại SP</th>
                                <th scope="col">Chọn</th>
                            </tr>
                            </thead>
                            <tbody>
                        
                            </tbody>
                            </<tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
 <script src="/js/fm_money/simple.money.format.js"></script>
<script>
        $.ajaxSetup({
            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });


        var countProductForm = $('#row-create-product').find('.productForm').length + 1;
        ;

        function createProductForm() {
            $('#row-create-product').append(
                '<div class="form-group col-md-12 row productForm">'
                + '<div class="col-md-6">'
                + '<label for="">Tên sản phẩm </label>'
                + '<input type="text" class="form-control form-control-user" '
                + 'name="[' + countProductForm + '][name]" value="" placeholder="Tên Sản phẩm ">'
                + '<input type="hidden" name="product[' + countProductForm + '][name]" value=""/>'
                + '</div>'
                + '<div class="col-md-6">'
                + '<label for="">Số lượng </label>'
                + '<input type="text" class="form-control form-control-user" id=""'
                + 'name="[' + countProductForm + '][quantity]" value="" placeholder="Số lượng SP ">'
                + '<input type="hidden" name="product[' + countProductForm + '][quantity]" value=""/>'
                + '</div>'
                + '</div>')
            countProductForm = countProductForm + 1;
        }

        $(".btn-find-product").click(function (e) {
            e.preventDefault();
            var pd_name = $('.find-product-name').val();
            var pd_cate_id = $('.find-category_product_id').val();
            var form_data = new FormData();
             form_data.append("pd_name",pd_name);
             form_data.append("pd_cate_id",pd_cate_id);
            $.ajax({
                url: "{{route('admin.order.searchproductname')}}",
                data: form_data,
                type: 'POST',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (res) {
                    $('#tab-search tbody').html('');
                   $('#tab-search').append(res.html);
                },error: function (error) {
                	console.log(error);
                }

            })
        });

        function getProduct(content) {
            if($('#row-create-product ').find('.id-product-'+$(content).data('id')+'').data('id') ==  $(content).data('id') )
            {
                 var num = +$('#row-create-product .product-quantity-'+$(content).data('id')+'').val() + 1 ;
                 $('#row-create-product .product-quantity-'+$(content).data('id')+'').val(num);
            }else{
                  $('#row-create-product').append(
                '<div class="form-group col-md-12 row productForm">'
                + '<div class="col-md-4">'
                + '<label for="">Tên sản phẩm </label>'
                + '<input type="text" class="form-control form-control-user" '
                + 'name="product['+countProductForm+']" value="'+$(content).data('name')+'" placeholder="Tên Sản phẩm " disabled>'
                +'</div>'
                + '<div class="col-md-4">'
                + '<label for="">Đơn Giá / S.Phẩm (VNĐ) </label>'
                + '<input type="text" class="form-control form-control-user  product-price-'+ $(content).data('id') +'" '
                + 'name="price['+countProductForm+']" value="'+$(content).data('price')+'" placeholder="" disabled>'
                +'</div>'
                + '<div class="col-md-4">'
                + '<label for="">Số Lượng </label>'
                + '<input type="number" class="form-control form-control-user only-numeric product-quantity-'+ $(content).data('id') +'" id="product-quantity"'
                + 'name="products['+countProductForm+'][quantity]" value="1" onkeypress="validNumber(this)" placeholder="" >'
                +  '<span class="error" style="color: red; display: none">* Vui lòng nhập số (0 - 9)</span>'
                +'</div>'
                + '<input type="hidden" class="form-control form-control-user id-product-'+ $(content).data('id') +'" '
                + 'name="products['+countProductForm+'][id]" data-id="'+ $(content).data('id') +'" value="'+ $(content).data('id') +'" placeholder="" >'
                +'</div>')
            }
             $('.product-price-'+ $(content).data('id')+'').simpleMoneyFormat();  
          countProductForm = countProductForm + 1;
      };
    </script>
@endsection