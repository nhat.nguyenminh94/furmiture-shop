<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/user-auth/css/main.css">
</head>
<body>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Tabs Titles -->

        <!-- Icon -->
        <div class="fadeIn first">
            <i style="font-size:24px" class="fa">&#xf007;</i>
        </div>

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <!-- Login Form -->
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <input type="text" id="email" class="fadeIn second" name="email" placeholder="Nhập Email của bạn"
                   value="{{ old('email') }}" required autocomplete="email" autofocus>
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="Mật Khẩu" required
                   autocomplete="current-password">
            <input type="submit" class="fadeIn fourth" value="Đăng Nhập">
        </form>

        <!-- Remind Passowrd -->
        <div id="formFooter">
            <div clas="col-md-12 ">
                <a class="underlineHover" href="#">Forgot Password?</a>
            </div>
            <div class="col-md-12 ">
                <a class="underlineHover" href="/register">Đăng Ký</a>
            </div>
             <div class="col-md-12 ">
                <a class="underlineHover" href="">Về Trang chủ</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>