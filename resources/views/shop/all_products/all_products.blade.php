@extends('shop.layouts.master',[ 'single_page' =>'true' ])
@section('styles')
@endsection
@section('content')
 <section class="shop">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="shop-products">
							@foreach($products as $product)
                            <div class="col-md-4 col-sm-6">
                                <div class="product">
                                    <a href="{{route('shop.detail',['id'=>$product->id])}}"><img src="{{url($product->images[0]->src ?? '/shop/assets/images/product/01.jpg')}}" alt="">
                                        <h2 class="product-title text-center">{{$product->name}}</h2>
                                        <div class="overlay"></div>
                                    </a>
                                    <div class="product-info">
                                        <span class="product-price pull-left">{{number_format($product->price,0,',',',')}} VNĐ</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
              <div class="col-12">
                {{ $products->links() }}
            </div>
        </div>
    </section>
@endsection