@extends('shop.cart.layouts_cart',['nameMenu'=>'Lịch sử giao dịch'])
@section('content-cart')
    @if(!$usersOrder->first())
        <div class="alert alert-danger col-md-12 row" role="alert">
            Chưa phát sinh đơn hàng nào !
        </div>
    @else
    <div class="container">
        <div class="col-md-12 row">
            <p>Thông tin các đơn hàng của bạn đã được hệ thống lưu lại</p>
        </div>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Mã đơn hàng</th>
                <th>Ngày đặt hàng</th>
                <th>Tổng tiền</th>
                <th>Trạng thái đơn hàng</th>
            </tr>
            </thead>
            <tbody>
            @foreach($usersOrder as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{date('d-m-Y', strtotime($order->order_date))}}</td>
                    <td>{{ number_format($order->total,0,',','.') }}</td>
                    @if($order->status == 2)
                        <td>Đơn hàng đã được giao thành công</td>
                    @elseif($order->status == 3)
                        <td>Đơn hàng đã được hủy</td>
                    @else
                        <td>Đang xử lý đơn hàng</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endif
@endsection