@extends('shop.cart.layouts_cart',['nameMenu'=>'Thông tin đơn hàng'])
@section('content-cart')
    @if(!$products->first())
        <div class="col-md-12 cart_content_list">
            <div class="alert alert-danger text-center" role="alert">
                Giỏ hàng chưa có sản phẩm !
            </div>
        </div>
    @else
        <div class="col-md-12 cart_content_list">
            @foreach($products as $product)
                <div class="row cart_item">
                    <div class="col-lg-2 col-md-12">
                        <div class="cart_item_image">
                            <img src="{{$product->options->image ??  '/shop/assets/images/product/single.jpg'}}"
                                 alt="">
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        <div class="cart_item_name cart_info_col">
                            <div class="cart_item_title">Name</div>
                            <div class="cart_item_text">{{$product->name}}</div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        <div class="cart_item_quantity cart_info_col">
                            <div class="cart_item_title">Quantity</div>
                            <div class="cart_item_text">{{ $product->qty }}</div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        <div class="cart_item_price cart_info_col">
                            <div class="cart_item_title">Price</div>
                            <div class="cart_item_text">{{ number_format($product->price,0,',','.') }}</div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        <div class="cart_item_total cart_info_col">
                            <div class="cart_item_title">Total</div>
                            <div class="cart_item_text">{{ number_format($product->total,0,',','.') }}</div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        <div class="cart_item_total cart_info_col">
                            <div class="cart_item_title">Xóa</div>
                            <div class="cart_item_text">
                                <button type="button" class="btn btn-danger btnRemoveItem"
                                        onclick="removeItemCart('{{$product->rowId}}',this)"
                                        data-id="{{$product->rowId}}">
                                    Xóa
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
    <div class="col-md-12">
        <div class="order_total">
            <div class="order_total_content text-md-right">
                <div class="order_total_title">Tổng Tiền Giỏ Hàng :</div>
                <div class="order_total_amount">{{Cart::total()}} VNĐ</div>
            </div>
        </div>
    </div>
    <div class="col-md-12 cart_title">
        Thông Tin Khách Hàng
    </div>
    <div class="col-md-12">
        <form action="{{route('shop.cart.addcart')}}" method="post" id="contact_form">
            @csrf
            <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
                <input type="text" id="shipment_address" name="shipment_address"
                       class="contact_form_email input_field" placeholder="Địa chỉ giao hàng" required
                       autocomplete="shipment_address">
                <input type="text" id="shipment_name" name="shipment_name"
                       class="contact_form_email input_field" required placeholder="Tên người nhận"
                       required="required" data-error="shipment_name is required.">
                <input type="text" id="phone" name="phone"
                       class="contact_form_email input_field" required placeholder="Số điện thoại"
                       required="required" data-error="phone is required.">
            </div>
            <div class="contact_form_button">
                <button type="submit" class="btn btn-info" {{!$products->first() ? 'disabled' : ''}}>Thanh Toán</button>
            </div>
        </form>
    </div>
@endsection