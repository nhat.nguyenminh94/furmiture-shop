
<div class=" sidebar menu-info-container">
    <div class="mini-submenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
    <div class="list-group">
        <span href="#" class="list-group-item list-group-item-parent">
          Xin Chào {{(Auth::user()->name)}}
            <span class="pull-right" id="slide-submenu">
                <i class="fa fa-times"></i>
            </span>
        </span>
<!--         <a href="#" class="list-group-item">
            <i class="fa fa-comment-o"></i> Lorem ipsum
        </a> -->
        <a href="{{route('shop.cart.historyorder')}}" class="list-group-item">
            <i class="fa fa-search"></i> Lịch sử giao dịch
        </a>
        <a href="{{route('shop.info.showinfo')}}" class="list-group-item">
            <i class="fa fa-user"></i> Thông tin tài khoản
        </a>
        <a href="{{route('shop.cart.listcart')}}" class="list-group-item">
            <i class="fa fa-bar-chart-o"></i> Thông tin đơn hàng<span class="badge">{{Cart::count()}}</span>
        </a>
    </div>        
</div>