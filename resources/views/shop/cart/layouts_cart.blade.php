@extends('shop.layouts.master')
@section('styles')
    <style type="text/css">
    
    </style>
    @yield('styles-layouts-cart')
@endsection
@section('content')
	<section class="container cart_container">
        <div class="row">
        	 <div class="col-md-4">
                @include('shop.cart.menu-cart')
            </div>
             <div class="col-md-8">
                <div class="col-md-12 cart_title">
                   @if($nameMenu)
                   {{$nameMenu}}
                   @else
                        Thông Tin
                    @endif
            	</div>
            	@yield('content-cart')
            </div>	
        </div>
    </section>
@endsection