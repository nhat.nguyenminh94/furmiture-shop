@extends('shop.layouts.master')
@section('content')
 <section class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- Filters -->
                    <div class="portfolioFilter text-center">
                        <span>
                            <a href="#" data-filter="*" class="current">All</a>
                            @foreach($categories as $category)
                             <a href="#" data-filter=".{{$category->name}}">{{$category->name}}</a>
                            @endforeach
                        </span>
                    </div>
                </div>

                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="portfolioContainer">
                            @foreach($products as $product)
                            <div class="col-md-4 col-sm-6 col-xs-12 {{$product->category->name}}">
                                <div class="st-work">
                                    <a href="{{route('shop.detail',['id'=>$product->id])}}"><img src="{{$product->images[0]->src ??  '/shop/assets/images/portfolio/1.jpg'}}" alt=""><div class="overlay"></div></a>
                                    <div class="work-overlay text-center">
                                        <a href="#" class="like-button"><i class="fa fa-heart-o"></i></a>
                                        <h2><a href="single-portfolio.html">{{$product->name}}</a></h2>
                                        <h3 class="portfolio-cat"><a href="#">{{$product->category->name}}</a></h3>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection