<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="logo pull-left"><a href="{{route('shop.index')}}">
                    <img src="/shop/assets/images/logo.png"
                                                                                   alt="Sun"></a></div>
                <nav class="main-menu pull-right">
                    <ul class="menu-list">
                        <li class="menu-list-item"><a href="{{route('shop.index')}}">Trang Chủ</a></li>
                        <li class="menu-list-item"><a href="{{route('shop.allproducts')}}">Sản phẩm</a></li>
                        <li class="menu-list-item"><a href="{{route('shop.allarticles')}}">Tin Tức</a></li>
                        <li class="menu-list-item"><a href="contact.html">Liên Hệ</a></li>

                        @if(!Auth::check())
                            <li class="menu-list-item">
                                <div class="user_icon"><img src="/shop/images/user.svg" alt=""></div>
                                <div><a href="/register">Đăng Ký</a></div>
                            </li>
                            <li class="menu-list-item">
                                <div><a href="/login">Đăng Nhập</a></div>
                            </li>
                        @else
                            <li class="menu-list-item">
                                <img src="/shop/assets/images/user.png" alt="">
                                <a href="{{route('shop.info.showinfo')}}">{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}</a>
                              
                            </li>
                            <li class="menu-list-item">
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endif
                    </ul>
                </nav>

                <div class="menu-mobile"></div>
            </div>
        </div>
    </div>
</header>
@if(Auth::check())
    <div class="cart_header container">
        <div class="row cart_container_icon">
            <div class="col-md-10 col-md-offset-1">
                <div class="cart_icon" data-toggle="modal" data-target="#exampleModalLong"
                     onclick="ajaxShowCart(this)">
                    <img src="/shop/assets/images/cart.png" class="img-rounded" alt="">
                </div>
                <div class="cart_content">
                    <div class="cart_text"><a href="{{route('shop.cart.listcart')}}">Giỏ Hàng <span>( {{Cart::count()}}
                                )</span></a>
                    </div>
                    <div class="cart_count"></div>
                </div>
            </div>
        </div>
    </div>
@endif