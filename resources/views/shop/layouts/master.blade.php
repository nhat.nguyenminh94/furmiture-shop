<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Rubel Miah">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- favicon icon -->
    <link rel="shortcut icon" href="/shop/assets/images/logo.png">

    <title>Sun</title>

    <!-- common css -->
    <link rel="stylesheet" href="/shop/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/shop/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/shop/assets/css/slicknav.css">
    <link rel="stylesheet" href="/shop/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/shop/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="/shop/style.css">
    <link rel="stylesheet" href="/shop/assets/css/responsive.css">

    <base href="/">
    <style type="text/css">
        a:focus,
        button:focus,
        input:focus,
        textarea:focus {
            outline: none;
        }
    </style>
    <!-- <link rel="stylesheet" href="/shop/assets/css/cart_styles.css"> -->

    <!-- HTML5 shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/shop/assets/js/html5shiv.js"></script>
    <script src="/shop/assets/js/respond.js"></script>
    <![endif]-->
    @yield('styles')
</head>
<body>

<!--start pre-loader-->
<div id="st-preloader">
    <div class="st-preloader-circle"></div>
</div>
<!--end pre-loader-->

<!--start header-->
@include('shop.layouts.header')
<!--end header-->

<!--start banner-->
<section class="banner home-banner">
    <div class="overlay"></div>
    <div class="banner-text text-center text-uppercase">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h1>Welcome to Sun Website</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end banner-->

<!--start portfolio-->
@yield('content')
<!--end portfolio-->

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Giỏ Hàng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="dataAjaxCart">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--start footer-->
@include('shop.layouts.footer')
<!--end footer-->

<!-- js files -->
<script type="text/javascript" src="/shop/assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="/shop/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/shop/assets/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="/shop/assets/js/jquery.slicknav.js"></script>
<script type="text/javascript" src="/shop/assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/shop/assets/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="/shop/assets/js/scripts.js"></script>
<script type="text/javascript" src="/shop/js/main.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    console.log(location.href);
    $('.menu-list a[href^="' + location.href + '/"]').parent().addClass('active');
</script>
@yield('scripts')
</body>
</html>