  <footer class="footer">
        <div class="footer-bottom text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <p class="footer-address">
                            Sun Nguyễn 
                        </p>
                        <div class="footer-social-icons">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-facebook-f"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                        </div>
                        <div class="copy-right-text">&copy; 2016 NEON, Designed by <a href="#">Sun Dev</a> in VietNam</div>
                    </div>
                </div>
            </div>
        </div>
    </footer>