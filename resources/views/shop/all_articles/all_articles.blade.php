@extends('shop.layouts.master',[ 'single_page' =>'true' ])
@section('styles')
@endsection
@section('content')
  <div class="blog-section">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="blog-posts">
                        <div class="row">
                        	@foreach($articles as $article)
                            <div class="col-md-6">
                                <article class="blog-article">
                                    <a href="{{route('shop.detailarticle',['id'=>$article->id])}}"><img src="{{url($article->images[0]->src ?? '/shop/assets/images/blog/01.jpg')}}" alt="">
                                        <span class="post-date text-uppercase">{{\Carbon\Carbon::parse($article->created_at)->format('d M yy')}}</span>
                                        <span class="overlay"></span>
                                    </a>
                                    <div class="post-info text-center text-uppercase">
                                        <div class="post-cat"><a href="">{{$article->category->name}}</a></div>
                                        <h2 class="post-title"><a href="{{route('shop.detailarticle',['id'=>$article->id])}}">{{$article->title}}</a></h2>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                         
                            <div class="col-md-12 pagination text-center">
                                {{ $articles->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             
        </div>
    </div>
@endsection