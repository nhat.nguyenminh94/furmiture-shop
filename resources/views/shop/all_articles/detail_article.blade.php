@extends('shop.layouts.master',[ 'single_page' =>'true' ])
@section('styles')
@endsection
@section('content')
<div class="single-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-md-8">
                        <article class="single-post">
                            <a href="" class="post-thumb"><img src="/shop/assets/images/blog/single-01.jpg" alt=""></a>

                            <div class="post-meta text-center text-uppercase"><a href="" class="post-cat">{{$article->category->name}}</a>
                            </div>
                            <h2 class="post-title text-center text-uppercase">{{$article->title}}</h2>

                            <div class="post-content">
                                {!! $article->content !!}
                            </div>

                            <!--start tags-->
                            <?php $tagname = explode(",", $article->tag);
                ?>
                            <div class="entry-tags text-uppercase">
                               @foreach($tagname as $tag)
                               <a href="#">{{$tag}}</a>
                               @endforeach
                           </div>
                            <!--end tags-->

                            <!--start related post-->
                            <div class="related-post">
                                <h4 class="related-post-title">Related Post</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <article class="blog-article">
                                            <a href=""><img src="/shop/assets/images/blog/03.jpg" alt="">
                                                <span class="overlay"></span>
                                            </a>
                                            <div class="post-info text-center text-uppercase">
                                                <h2 class="post-title"><a href="">How to Shape Better UI?</a></h2>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-md-6">
                                        <article class="blog-article">
                                            <a href=""><img src="/shop/assets/images/blog/02.jpg" alt="">
                                                <span class="overlay"></span>
                                            </a>
                                            <div class="post-info text-center text-uppercase">
                                                <h2 class="post-title"><a href="">How to Shape Better UI?</a></h2>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!--end related post-->

                            <!--Start Comment Form-->
                            <div class="comment-form">
                                <h4 class="comment-form-title">Leave a Comment</h4>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 name"><input type="text" placeholder="Name"></div>
                                        <div class="col-md-6"><input type="text" placeholder="Email"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12"><input type="text" placeholder="Website"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12"><textarea name="comment" id="comment" cols="40" rows="6" placeholder="Comment"></textarea></div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" value="Submit">
                                    </div>
                                </div>
                            </div>
                            <!--End Comment Form-->
                        </article>
                    </div>
                    <div class="col-md-4">
                        <div class="primary-sidebar">
                          

                            <aside class="widget">
                                <h3 class="widget-title text-uppercase">Danh Mục Tin Tức</h3>
                                <ul>
                                    @foreach($categories as $category)
                                     <li><a href="">{{$category->name}} ({{$category->article->count()}})</a></li>
                                    @endforeach
                                </ul>
                            </aside>


                            <aside class="widget recent-post-widget">
                                <h3 class="widget-title text-uppercase">Recent Post</h3>
                                <ul class="posts-list">
                                    <li>
                                        <article class="media clearfix">
                                            <div class="entry-img media-left">
                                                <a href="">
                                                    <img src="/shop/assets/images/blog/recent-01.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h3 class="entry-title"><a href="">Evening Family Tea Table Party</a></h3>
                                                <div class="entry-date">20 August, 2016</div>
                                            </div>
                                        </article>
                                    </li>
                                    <li>
                                        <article class="media clearfix">
                                            <div class="entry-img media-left">
                                                <a href="">
                                                    <img src="/shop/assets/images/blog/recent-02.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h3 class="entry-title"><a href="">Evening Family Tea Table Party</a></h3>
                                                <div class="entry-date">20 August, 2016</div>
                                            </div>
                                        </article>
                                    </li>
                                    <li>
                                        <article class="media clearfix">
                                            <div class="entry-img media-left">
                                                <a href="">
                                                    <img src="/shop/assets/images/blog/recent-03.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h3 class="entry-title"><a href="">Evening Family Tea Table Party</a></h3>
                                                <div class="entry-date">20 August, 2016</div>
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
