@extends('shop.cart.layouts_cart',['nameMenu'=>'Thông tin tài khoản'])
@section('content-cart')
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
                @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <form method="post" action="{{route('shop.info.changeinfo')}}">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                       value="{{$user->email}}" disabled="disabled">
                <small id="emailHelp" class="form-text text-muted">Chúng tôi sẽ không bao giờ chia sẻ email của bạn với
                    bất cứ ai khác.
                </small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tên hiển thị</label>
                <input type="text" class="form-control" name="name" aria-describedby="emailHelp" value="{{$user->name}}"
                       required="required">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Địa chỉ</label>
                <input type="text" class="form-control" name="address" aria-describedby="emailHelp"
                       value="{{$user->address}}" required="required">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Số điện thoại</label>
                <input type="text" class="form-control" name="phone" aria-describedby="emailHelp"
                       value="{{$user->phone}}" required="required">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" class="" onclick="showPasswordForm()" style="cursor: pointer;">Thay đổi
                    mật
                    khẩu</label>
                <div class="col-md-12 container_password active" id="container_password">

                </div>
            </div>
            <input type="hidden" class="form-control" name="id" value="{{$user->id}}">
            <button type="submit" class="btn btn-info">Submit</button>
        </form>
    </div>
@endsection