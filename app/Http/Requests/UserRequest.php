<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Http\Requests\UserRequest;
use Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'address' => 'required',
            'phone' => 'required|numeric',
        ];

        if ($this->has('password_confirmation') && $this->has('old_password') && $this->has('password')) {
            if (!is_null($this->password_confirmation) && !is_null($this->password_confirmation) && Hash::check($this->old_password, Auth::user()->password)) {
                $rules = array_merge($rules, [
                    'password' => 'confirmed',
                ]);
            }
        }else{
              $rules = array_merge($rules,[
                    'old_password' => 'required',
                    'password' => 'required|min:6',
                    'password_confirmation' => 'required|min:6',]);   
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'tên tài khoản không được để trống',
            'name.max' => 'tên tài khoản chỉ được 255 ký tự',
            'address.required' => 'địa chỉ không được để trống',
            'phone.required' => 'số điện thoại không được để trống',
            'phone.numeric' => 'số điện thoại phải là số',
            'old_password.required' => 'Please enter old password',
            'password.required' => 'Please enter password',
            'password_confirmation.required' => 'Please enter password confirmation',
            'password.confirmed' => 'Xác nhận mật khẩu ko trùng khớp',
        ];
    }
}
