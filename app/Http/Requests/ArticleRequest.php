<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'file_uploads'       => 'required',
            'title'       => 'required|max:255',
            'sub_title'       => 'required',
            'content'       => 'required',
            'status'       => 'required',
            'category_article_id'       => 'required',
        ];
    }
     public function messages()
    {
        return [
             'file_uploads.required'       => 'Image không được để trống',
            'title.required'       => 'title không được để trống',
            'sub_title.required'       => 'sub title không được để trống',
            'status.required'       => 'status không được để trống',
            'category_article_id.required'       => 'category article  không được để trống',
            'content.required'       => 'content không được để trống',
        ];
    }
}
