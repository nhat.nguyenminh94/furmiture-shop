<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {

        Validator::extend('number_check', function($attribute, $value, $parameters, $validator) {
              $value = $this->price;
              $value = (int) preg_replace('/[^A-Za-z0-9\-]/', '', $value);
              if(is_int($value)  == true && $value > 0){ 
                    return true;
                }else{
                    return false;
                }
            });
         $price = (int) preg_replace('/[^A-Za-z0-9\-]/', '', $this->price);
          $this->merge(['price' => $price]);
        return [
            'file_uploads'       => 'required',
            'name'       => 'required|max:255',
            'price'       => 'required|number_check',
            'detail'       => 'required',
            'description'       => 'required',
            'status'       => 'required',
            'category_product_id'       => 'required',
        ];
    }
     public function messages()
    {
        return [
             'file_uploads.required'       => 'Image không được để trống',
            'name.required'       => 'name không được để trống',
            'price.required'       => 'giá không được để trống',
            'price.number_check'       => 'giá phải là số ',
            'status.required'       => 'status không được để trống',
            'category_product_id.required'       => 'category article  không được để trống',
            'detail.required'       => 'detail không được để trống',
            'description.required'       => 'description không được để trống',
        ];
    }
}
