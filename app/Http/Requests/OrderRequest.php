<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'shipment_address'       => 'required|max:255',
            'shipment_name'       => 'required',
            'phone'       => 'required',
            'status'       => 'required',
            'products'       => 'required',
        ];
    }

     public function messages()
    {
        return [
            'shipment_address.required'       => 'Địa chỉ không được để trống',
            'shipment_name.required'       => 'Tên người nhận không được để trống',
            'status.required'       => 'Trạng thái không được để trống',
            'phone.required'       => 'SDT không được để trống',
            'products.required'       => 'Vui lòng chọn sản phẩm để tạo đơn',
        ];
    }
}
