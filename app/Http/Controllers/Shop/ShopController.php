<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategoryProduct;
use App\Models\CategoryArticle;
use App\Models\Product;
use App\Models\Article;
use App\Models\Order;
use App\User;
use App\Models\OrderDetail;
use Cart;
use Carbon\Carbon;
use Response;
use View;
use Auth;
use Hash;
use App\Http\Requests\UserRequest;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CategoryProduct::where('status', 1)->get();
        $products = Product::where('status', 1)->get();
        return view('shop.index', ['categories' => $categories, 'products' => $products]);
    }

    public function allProducts()
    {
        $products = Product::select()->where('status', 1)->paginate(5);
        return view('shop.all_products.all_products', ['products' => $products]);
    }

    public function allArticles()
    {
        $articles = Article::select()->where('status', 1)->paginate(5);
        return view('shop.all_articles.all_articles', ['articles' => $articles]);
    }

    public function detailArticle($id)
    {
        $article = Article::findOrFail($id);
        $categories = CategoryArticle::all();
        return view('shop.all_articles.detail_article', ['article' => $article, 'categories' => $categories]);
    }

    public function addProductToCart(Request $request)
    {
        $product = Product::find($request->id);
        if ($product) {
            Cart::add(
                ['id' => $request->id,
                    'name' => $product->name,
                    'qty' => $request->quantity_input,
                    'price' => $product->price,
                    'weight' => 550,
                    'options' => ['image' => $product->images->first()->src ?? 'error'
                    ]]);
        }
        return redirect()->back();
    }

    public function ajaxShowCart()
    {
        $products = Cart::Content();
        $total = Cart::total();
        return Response::json(View::make('shop.cart.show-ajax-cart', array('items' => $products, 'total' => $total))->render());
    }

    public function listCartProduct()
    {
        $products = Cart::content();
        $viewData = [
            'products' => $products
        ];
        return view('shop.cart.show-cart', ['products' => $products]);
    }

    public function removeItemCart(Request $request)
    {
        Cart::remove($request->rowId);
        $status = 'true';
        $totalNew = Cart::total();
        $msg[] = ['status' => $status,
            'totalNew' => $totalNew];
        return $msg;
    }

    public function addCart(Request $request)
    {
        $products = Cart::Content();
        if (count($products) == 0) {
            return redirect()->back()->with('status', 'Chưa có sản phẩm trong giỏ hàng');
        }
        if ($request->use_user == "on") {
            $orderAdd['name'] = auth()->user()->name;
            $orderAdd['phone'] = auth()->user()->phone;
            $orderAdd['delivery_address'] = auth()->user()->address;
        } else {
            $orderAdd = $request->all();
        }
        $orderAdd['order_date'] = Carbon::now()->toDateTimeString();
        $orderAdd['user_id'] = auth()->user()->id;
        $total = Cart::total();
        $result = str_replace(",", "", $total);
        $orderAdd['total'] = (int)$result;
        $orderAdd['status'] = 1;
        $order = Order::create($orderAdd);
        foreach ($products as $product) {
            $order->orderDetails()->create(['product_id' => $product->id, 'order_id' => $order->id, 'quantity' => $product->qty, 'until_price' => $product->price]);
        }
        Cart::destroy();
        return redirect()->route('shop.index')->with('status', 'Đơn Hàng của bạn đã được lưu!');
    }

    public function historyOrder()
    {
        $usersOrder = Order::where('user_id', auth()->user()->id)->get();
        return view('shop.cart.history-order', ['usersOrder' => $usersOrder]);
    }


    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('shop.detail.detail', ['product' => $product]);
    }

    public function showInfo()
    {
        $user = auth()->user();
        return view('shop.info-user.info-user', ['user' => $user]);
    }

    public function changeInfo(UserRequest $request)
    {
        $user = User::findOrFail($request->id);
        $request_data = $request->all();
        if (!is_null($request_data['password_confirmation']) && !is_null($request_data['password_confirmation'])) {
            $current_password = Auth::User()->password;
            if (Hash::check($request_data['old_password'], $current_password)) {
                $user->password = Hash::make($request_data['password']);;
                $request_data = ['password', $user->password];
            } else {
                return redirect()->back()->with('error', 'Mật khẩu cũ không đúng');
            }
        }
        $user->update($request_data);
        return redirect()->back()->with('message', 'Cập nhật thông tin tài khoản thành công !!');
    }
}
