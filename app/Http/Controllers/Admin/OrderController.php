<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\CategoryProduct;
use Response;
use Carbon\Carbon;
use App\Http\Requests\OrderRequest;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(5);
        return view('admin.order.list', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategoryProduct::where('status', 1)->get();
        return view('admin.order.create', ['categories' => $categories]);
    }


    public function searchProductName(Request $request)
    {
        $query = Product::select();
        if ($request->pd_name != null) {
            $query = $query->where('products.name', 'like', '%' . $request->pd_name . '%');
       }
        if ($request->pd_cate_id != null && $request->pd_cate_id != "0") {
            $query = $query
                ->join('category_products', 'products.category_product_id', '=', 'category_products.id')
                ->where('category_products.id', $request->pd_cate_id);
        }
        $searchProductAll = $query->get();
        // $searchProductData = $query->paginate(1);
        $returnHTML = view('admin.order.search-product',['searchProductAll'=> $searchProductAll])->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $totalProducts = 0 ;
        // dd($request->received_date);
        $orderAdd = $request->all();
        foreach ($request->products as $value) {
             $productDetail = Product::findOrFail($value['id']);
             $sumProduct = $productDetail->price * $value['quantity'];
             $totalProducts += $sumProduct;
          }
        $orderAdd['total'] = $totalProducts;  
        $orderAdd['order_date'] = Carbon::now()->toDateTimeString();
        $orderAdd['user_id'] = auth()->user()->id;
        $orderAdd['received_date'] = Carbon::parse($request->received_date)->toDatetimelocalString();

        $order = Order::create($orderAdd);
        foreach ($request->products as $product) {
             $productDetail = Product::findOrFail($product['id']);
            $order->orderDetails()->create(['product_id' => $productDetail->id, 'order_id' => $order->id, 'quantity' => $product['quantity'], 'until_price' => $productDetail->price]);
        }
        return redirect()->route('admin.order.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = CategoryProduct::where('status', 1)->get();
        $order = Order::findOrFail($id);
        $order->received_date = Carbon::parse($order->received_date)->format('Y-m-d\TH:i');
        // $orderDate = Carbon::createFromFormat('Y-m-d\TH:i',$order->received_date);
        return view('admin.order.update',['order'=> $order, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
