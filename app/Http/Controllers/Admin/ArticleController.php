<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategoryArticle;
use App\Models\Article;
use App\Http\Requests\ArticleRequest;
use Validator;
use App\Models\Image;
use File;
class ArticleController extends Controller
{
     protected $categories;

    public function __construct()
    {
        $this->categories = CategoryArticle::select()->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $articles = Article::select()->paginate(5);
        return view('admin.article_management.article.list',
            [
                'articles' => $articles
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $categories = CategoryArticle::all();
        return view('admin.article_management.article.create',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $this->handleStore($request);
        return redirect()->route('admin.article.list');
    }

       public function handleStore($request)
    {
         // dd($request->all());

        $dataInput = array_merge($request->all(), [
            'user_id' => 1,
        ]);
        $article = Article::create($dataInput);
        foreach ($request->file_uploads as $key => $val) {
            $dataImage['src'] = $val['src'];
            $dataImage['alt'] = str_slug($request->title)."-hinh-".$key;
            $dataImage['description'] = str_slug($request->title)."-hinh-".$key;
            $article->images()->create($dataImage);
        }
        return $article;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $categories = $this->categories;
        $product = Article::findOrFail($id);
        return view('admin.article_management.article.update', ['article' => $product,
            'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $model = Article::findOrFail($id);
        $this->handleUpdate($request, $model);
        return redirect()->route('admin.article.list');
    }

      public function handleUpdate($request, $model)
    {
        // dd($request->all());
        $dataInput = array_merge($request->all(), [
            'user_id' => auth()->user()->id,
        ]);
        $article = $model->update($dataInput);
        $imagesModel = $model->images()->pluck('id')->toArray();
        $dataRequest = $request->file_uploads;
        foreach ($dataRequest as $key => $value) {
            if (isset($value['id'])) {
                $idArticle = $value['id'];
                $imageUpdate = $model->images()->find($idArticle);
                $oldImageSrc = $imageUpdate->src;
                $pos = array_search($value['id'], $imagesModel);
                unset($value['id']);
                if ($imageUpdate->update(['src' => $value['src']])) {
                    if ($oldImageSrc != $value['src']) {
                        File::delete($oldImageSrc);
                    }
                }
                unset($dataRequest[$key]);
                unset($imagesModel[$pos]);
            } else {
                $model->images()->create(['src' => $value['src']]);
            }
        }
        if (sizeof($imagesModel) > 0) {
            foreach ($imagesModel as $itemDelete) {
                $imageDelete = $model->images()->find($itemDelete);
                  $oldImageSrc = $imageDelete->src;
                if ($imageDelete->delete()) {
                    File::delete($oldImageSrc);
                }
            }
        }
        return $article;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UploadMultiple(Request $request, $type = null)
    {
        // dd($request->file('files'));
        $success = [];
        $errors = [];
        foreach ($request->file('files') as $key => $file) {
            $handle = $this->handleUploadMultiple($request, $key, $type);
            if ($handle['status'] == false) {
                $errors[] = $handle;
            }
            if ($handle['status'] == true) {
                $success[] = $handle;
            }
        }
        return [
            'success' => $success,
            'errors' => $errors,
        ];
    }

    public function handleUploadMultiple($request, $key, $type = null)
    {
        $validator = Validator::make($request->all(),
            [
                'files.' . $key => 'image',
            ],
            [
                'files.' . $key . '.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($validator->fails()) {
            return [
                'status' => false,
                'file' => $request->file('files')[$key]->getClientOriginalName() ?? null,
                'errors' => $validator->errors()->getMessages()['files.' . $key],
            ];
        }
        $file = $request->file('files')[$key];
        if ($type) {
            $dir = 'uploads/' . $type . '/';
        } else {
            $dir = 'uploads/';
        }
        $date = date("dmY");
        $originName = $file->getClientOriginalName();
        $file_name = rand() . $date . '.' . $file->getClientOriginalExtension();
//        $file = request()->file('file');
//        $name = time() . $file->getClientOriginalName();
//        $filePath = 'products/' . $name;
//        Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
        if ($file->move($dir, $file_name)) {
            return ['status' => true, 'filename' => $dir . $file_name, 'filenameorigin' => $originName];
        }
        return ['status' => false, 'errors' => $originName];
    }
}
