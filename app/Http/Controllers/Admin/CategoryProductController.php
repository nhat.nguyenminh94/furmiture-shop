<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategoryProduct;
use App\Models\Product;
use App\Http\Requests\CategoryRequest;
class CategoryProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $categories = CategoryProduct::select()->paginate(5);
        return view('admin.product_management.category.list',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $categories = CategoryProduct::all();
        return view('admin.product_management.category.create',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        CategoryProduct::create($request->all());
        return redirect()->route('admin.categoryproduct.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         
        $category = CategoryProduct::findOrFail($id);
        // dd($categories);
        return view('admin.product_management.category.update',
            [
                'category' => $category,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
       $category = CategoryProduct::findOrFail($id);
        $dataInput = $request->all();
        $category->update($dataInput);
        return redirect()->route('admin.categoryproduct.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
          $category = CategoryProduct::findOrFail($request->idDelete);
        if($category->product()->count() <= 0 ){
            $category->delete(); 
        }
        return redirect()->route('admin.categoryproduct.list');
    }
}
