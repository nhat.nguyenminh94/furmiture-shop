<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategoryArticle;
use App\Models\Article;
use App\Http\Requests\CategoryRequest;
class CategoryArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $categories = CategoryArticle::select()->paginate(5);
        return view('admin.article_management.category.list',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $categories = CategoryArticle::all();
        return view('admin.article_management.category.create',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
       CategoryArticle::create($request->all());
        return redirect()->route('admin.categoryarticle.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $category = CategoryArticle::findOrFail($id);
        // dd($categories);
        return view('admin.article_management.category.update',
            [
                'category' => $category,
            ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
       $category = CategoryArticle::findOrFail($id);
        $dataInput = $request->all();
        $category->update($dataInput);
        return redirect()->route('admin.categoryarticle.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $category = CategoryArticle::findOrFail($request->idDelete);
        if($category->article()->count() <= 0 ){
            $category->delete(); 
        }
        return redirect()->route('admin.categoryarticle.list');
    }
}
