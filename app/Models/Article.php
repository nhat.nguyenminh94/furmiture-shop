<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable=['title','sub_title','content','status','category_article_id','user_id','tag'];
  	public function category()
   {
   		return $this->belongsTo('App\Models\CategoryArticle','category_article_id');
   }
     public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
