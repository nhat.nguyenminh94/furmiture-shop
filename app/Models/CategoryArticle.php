<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryArticle extends Model
{
     protected $fillable=['name','status'];

      public function article()
    {
    	return $this->hasMany(Article::class);
    }
}
