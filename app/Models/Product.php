<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     protected $fillable=['name','price','detail','status','category_product_id','user_id','description','discount_price'];
  	public function category()
   {
   		return $this->belongsTo('App\Models\CategoryProduct','category_product_id');
   }
     public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
     public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class,'product_id');
    }
    
}
