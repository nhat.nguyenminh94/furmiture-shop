<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '', 'namespace' => 'Shop', 'as' => 'shop.'], function () {
    Route::get('/', 'ShopController@index')->name('index');
    // Route::get('/the-loai/{slugParent}/{slug}', 'ShopController@categoryList')->name('categorylist');
    Route::get('/san-pham/chi-tiet/{id}', 'ShopController@show')->name('detail');
    Route::get('/all-products', 'ShopController@allProducts')->name('allproducts');
    Route::get('/all-articles', 'ShopController@allArticles')->name('allarticles');
    Route::get('/bai-viet/{id}', 'ShopController@detailArticle')->name('detailarticle');
    // Route::get('/category-list/{name}_{id}', 'ShopController@categoryList')->name('categorylist');
    Route::group(['prefix' => 'Cart', 'as' => 'cart.', 'middleware' => ['auth']], function () {
        Route::post('/add-product-cart', 'ShopController@addProductToCart')->name('addproducttocart');
        Route::get('/gio-hang-cua-ban', 'ShopController@listCartProduct')->name('listcart');
        Route::post('/remove-item-cart', 'ShopController@removeItemCart')->name('removeitemcart');
        Route::post('/add-cart', 'ShopController@addCart')->name('addcart');
        Route::get('/ajax-show-cart', 'ShopController@ajaxShowCart')->name('ajaxshowcart');
        Route::get('/lich-su-giao-dich', 'ShopController@historyOrder')->name('historyorder');
    });
    Route::group(['prefix' => 'Info', 'as' => 'info.', 'middleware' => ['auth']], function () {
        Route::get('/thong-tin-tk', 'ShopController@showInfo')->name('showinfo');
        Route::post('/change/thong-tin-tk', 'ShopController@changeInfo')->name('changeinfo');
    });
    // Route::post('/comment/add/{slug}_{id}', 'ShopController@addComment')->name('addcomment');
    // Route::get('/about-us', 'ShopController@aboutUs')->name('about');
    // Route::get('/contact-us', 'ShopController@contactUs')->name('contact');
    // Route::post('/contact-us/add', 'ShopController@addContact')->name('addcontact');
});
Route::get('/login-admin', 'Admin\AdminController@loginAdmin')->name('loginadmin');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => ['checktype']], function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::group(['prefix' => 'categoryproduct', 'as' => 'categoryproduct.'], function () {
        Route::get('/', 'CategoryProductController@index')->name('list');
        Route::get('/add', 'CategoryProductController@create')->name('add');
        Route::post('/created', 'CategoryProductController@store')->name('created');
        Route::get('edit/{id}', 'CategoryProductController@edit')->name('edit');
        Route::post('/{id}', 'CategoryProductController@update')->name('update');
        Route::delete('/deletecate', 'CategoryProductController@destroy')->name('destroy');
    });
    Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
        Route::get('/', 'ProductController@index')->name('list');
        Route::get('/add', 'ProductController@create')->name('add');
        Route::post('/created', 'ProductController@store')->name('created');
        Route::get('edit/{id}', 'ProductController@edit')->name('edit');
        Route::post('/{id}', 'ProductController@update')->name('update');
        Route::delete('/product/{id}', 'ProductController@destroy')->name('destroy');
        Route::post('/deleteimage', 'ProductController@deleteImage')->name('deleteimage');
        Route::post('/uploadimage/{type?}', 'ProductController@UploadMultiple')->name('uploadimage');
    });

    Route::group(['prefix' => 'categoryarticle', 'as' => 'categoryarticle.'], function () {
        Route::get('/', 'CategoryArticleController@index')->name('list');
        Route::get('/add', 'CategoryArticleController@create')->name('add');
        Route::post('/created', 'CategoryArticleController@store')->name('created');
        Route::get('edit/{id}', 'CategoryArticleController@edit')->name('edit');
        Route::post('/{id}', 'CategoryArticleController@update')->name('update');
        Route::delete('/deletecate', 'CategoryArticleController@destroy')->name('destroy');
    });
    Route::group(['prefix' => 'article', 'as' => 'article.'], function () {
        Route::get('/', 'ArticleController@index')->name('list');
        Route::get('/add', 'ArticleController@create')->name('add');
        Route::post('/created', 'ArticleController@store')->name('created');
        Route::get('edit/{id}', 'ArticleController@edit')->name('edit');
        Route::post('/{id}', 'ArticleController@update')->name('update');
        Route::delete('/article/{id}', 'ArticleController@destroy')->name('destroy');
        Route::post('/deleteimage', 'ArticleController@deleteImage')->name('deleteimage');
        Route::post('/uploadimage/{type?}', 'ArticleController@UploadMultiple')->name('uploadimage');
    });
     Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::get('/', 'OrderController@index')->name('list');
        Route::get('/add', 'OrderController@create')->name('add');
        Route::post('/searchproductname', 'OrderController@searchProductName')->name('searchproductname');
        Route::post('/created', 'OrderController@store')->name('created');
        Route::get('edit/{id}', 'OrderController@edit')->name('edit');
        Route::post('/{id}', 'OrderController@update')->name('update');
        Route::delete('/deletecate', 'OrderController@destroy')->name('destroy');
    });
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
