$(document).ready(function()
{
	initQuantity();
	/*
7. Init Quantity

	*/

	function initQuantity()
	{
		// Handle product quantity input
		if($('.product_quantity').length)
		{
			var input = $('#quantity_input');
			var incButton = $('#quantity_inc_button');
			var decButton = $('#quantity_dec_button');

			var originalVal;
			var endVal;

			incButton.on('click', function()
			{
				originalVal = input.val();
				endVal = parseFloat(originalVal) + 1;
				input.val(endVal);
			});

			decButton.on('click', function()
			{
				originalVal = input.val();
				if(originalVal > 0)
				{
					endVal = parseFloat(originalVal) - 1;
					input.val(endVal);
				}
			});
		}
	}

	
});

function ajaxShowCart(content){
	var dataHtml = ''
	$.ajax({
		url: '/Cart/ajax-show-cart',		
		type:'GET',
		contentType: false,
		processData: false,
		success: function( result ) {
			console.log(result);
			$(".dataAjaxCart").html(result);
		}
	});
	}

function removeItemCart(id,content)
{
	console.log(content);
	var cartItemRemove = $(content).parents('.cart_item');
	var form_data = new FormData();
	form_data.append("rowId",id);
	$.ajax({
		url: '/Cart/remove-item-cart',
		data: form_data,
		type:'POST',
		contentType: false,
		processData: false,
		success: function( msg ) {
			if(msg[0]['status'] = 'true' ){
				cartItemRemove.remove();
				$('.order_total_amount').html(msg[0]['totalNew'] + ' VNĐ');
			}
		}
	});
}


// info user function

 $('.menu-info-container a[href^="' + location.href + '"]').addClass('active');
  $('#slide-submenu').on('click',function() {                 
        $(this).closest('.list-group').fadeOut('slide',function(){
            $('.mini-submenu').fadeIn();    
        });
        
      });

    $('.mini-submenu').on('click',function(){       
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
    })


      function showPasswordForm() {
            if ($("#container_password").hasClass('active')) {
                $("#container_password").append(
                    '<label for="current-password" class="col-sm-4 control-label">'
                    +'Mật khẩu cũ'
                    +'</label>'
                    +'<div class="col-sm-8">'
                    +'<div class="form-group">'
                    +'<input type="password" class="form-control" id="old_password" name="old_password" placeholder="Password"></div>'
                    +'</div>'
                    +'<label for="password" class="col-sm-4 control-label">'
                    +'Mật khẩu mới'
                    +'</label>'
                    +'<div class="col-sm-8">'
                    +'<div class="form-group">'
                    +'<input type="password" class="form-control" id="password" name="password" placeholder="Password">'
                    +'</div>'
                    +'</div>'
                    +'<label for="password_confirmation" class="col-sm-4 control-label">Nhập lại Mật khẩu mới </label>'
                    +'<div class="col-sm-8">'
                    +'<div class="form-group">'
                    +'<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password">'
                    +'</div>'
                    +'</div>')
                $("#container_password").removeClass('active')
            }
            else {
                $("#container_password").html('');
                $("#container_password").addClass('active')
            }
        }