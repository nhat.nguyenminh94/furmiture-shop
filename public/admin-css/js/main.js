  (function ($, window, undefined) {
            //is onprogress supported by browser?
            var hasOnProgress = ("onprogress" in $.ajaxSettings.xhr());

            //If not supported, do nothing
            if (!hasOnProgress) {
                return;
            }

            //patch ajax settings to call a progress callback
            var oldXHR = $.ajaxSettings.xhr;
            $.ajaxSettings.xhr = function () {
                var xhr = oldXHR.apply(this, arguments);
                if (xhr instanceof window.XMLHttpRequest) {
                    xhr.addEventListener('progress', this.progress, false);
                }

                if (xhr.upload) {
                    xhr.upload.addEventListener('progress', this.progress, false);
                }

                return xhr;
            };
        })(jQuery, window);

        function UploadImageMultiple(e) {
            console.log(e);
            var form_data = new FormData();
            var ins = e.files.length;
            for (var x = 0; x < ins; x++) {
                form_data.append("files[]", e.files[x]);
            }
            $('.progressBarUploadImage').attr('aria-valuenow', 0).css('width', 0 + '%');
            $('.progressBarUploadImageShow').text(0 + '%');
            form_data.append('_token', "{{ csrf_token() }}");
            $.ajax({
                url: "",
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                progress: function (e) {
                    $('.progressBarUploadImageShow').parent().removeClass("bg-danger")
                    $('.progressBarUploadImageShow').parent().addClass("bg-success")
                    if (e.lengthComputable) {
                        var pct = (e.loaded / e.total) * 100;
                        $('.progressBarUploadImage').attr('aria-valuenow', parseInt(pct)).css('width', parseInt(pct) + '%');
                        $('.progressBarUploadImageShow').text(parseInt(pct) + '%');
                        if (parseInt(pct) == 100) {
                            $('.progressBarUploadImageShow').text('complete');
                        }
                    }
                },
                success: function (res) {
                    console.log(res);
                    if (res.errors.length != 0) {
                        $('.errorUploadImageResult').html(printErrorsMultiple(res.errors))
                    }
                    if (res.success.length != 0) {
                        $('.listUploadImageResult').append(printSuccessMultiple(res.success))
                    }
                    $(e).val('')
                }, error: function (error) {
                    $('.progressBarUploadImageShow').parent().removeClass("bg-success")
                    $('.progressBarUploadImageShow').parent().addClass("bg-danger")
                    $('.progressBarUploadImageShow').text('error');
                }
            });
        }

        function printErrorsMultiple(param) {
            var result = '<div class="alert alert-danger dismissible-alert" role="alert">'
                + ' <b>Errors:</b></i>'
                + ' <br>'
                + ' <ul class="list-unstyled">';
            param.forEach(function (element) {
                result = result + '<li>' + element.file + '</li>';
            });
            result = result + '</ul></div>';
            return result;
        }

        var countImageBlog = $('.listUploadImageResult').find('.media').length + 1;

        function printSuccessMultiple(param) {
            var result = '';
            param.forEach(function (element) {
                result = result + '<div class="media imageResultShow row">'
                    + '<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 article_image">'
                    + ' <img src="/' + element.filename + '" onclick="showOneImage(this)" data-src="/' + element.filename + '" data-atl="" data-height="100%" data-width="100%" class="mr-3" style="width:100%;max-width: 200px;">'
                    + '</div>'
                    + ' <div class="media-body col-xl-10 col-lg-10 col-md-10 col-sm-12 box_img">'
                    + ' <div class="form-group">'
                    + ' <div class="row col-md-12"> <div class="col-md-9 box_href">'
                    + ' <input type="text" class="form-control enable-mask inputSrc" value="' + location.protocol + "//" + window.location.host + '/' + element.filename + '" disabled>'
                    + ' </div><div class="col-md-3"><button type="button" class="btn btn-outline-danger" onclick="removePedding(this)" data-src="' + element.filename + '">Remove</button></div> '
                    + ' </div>'
                    + ' </div>'
                    + ' <input type="hidden" name="file_uploads[' + countImageBlog + '][src]" value="' + element.filename + '"/>'
                    + ' </div>'
                    + ' </div>'
                    + '<div style="font-size: 0;line-height: 0;margin: 10px 0;">&nbsp;</div>';
                countImageBlog = countImageBlog + 1;
            });
            return result;
        }


        function removePedding(e) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                buttons: {
                    cancel: true,
                    confirm: {
                        text: "Delete",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
            }).then((result) => {
                if (result == true) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    $(e).parents('.imageResultShow').remove();
                }
            });
        }
